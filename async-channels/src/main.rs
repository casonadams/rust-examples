use async_std::io;
use async_std::sync::channel;
use async_std::task;
use rand::Rng;
use std::time::Duration;

#[derive(Debug)]
struct Message {
    id: u64,
    sleep: u64,
    itr: u64,
}

async fn sleeper(m: Message, tx: async_std::sync::Sender<Message>) -> io::Result<()> {
    // need to use async sleeps or message will be missed
    task::sleep(Duration::from_millis(m.sleep)).await;
    tx.send(m).await;
    Ok(())
}

// Example of running many async threads
#[async_std::main]
async fn main() -> io::Result<()> {
    let (tx, rx) = channel(1);
    let mut rng = rand::thread_rng();
    for i in 0..10 {
        let a = Message {
            id: i.to_owned(),
            sleep: rng.gen_range(100, 500).to_owned(),
            itr: 1,
        };
        task::spawn(sleeper(a, tx.clone()));
    }

    loop {
        if let Ok(v) = rx.recv().await {
            println!("{:?}", v);
            let m = Message {
                id: v.id,
                sleep: rng.gen_range(100, 500).to_owned(),
                itr: v.itr + 1,
            };
            task::spawn(sleeper(m, tx.clone()));
        }
    }
}
