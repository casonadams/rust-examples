use rand::Rng;
use std::time::Duration;
use tokio::sync::mpsc;
use tokio::sync::mpsc::error::SendError;
use tokio::time::delay_for;

#[derive(Debug)]
struct Message {
    id: u64,
    sleep: u64,
    itr: u64,
}

async fn sleeper(
    m: Message,
    mut tx: mpsc::Sender<Message>,
) -> std::result::Result<(), SendError<Message>> {
    // need to use async sleeps or message will be missed
    delay_for(Duration::from_millis(m.sleep)).await;
    tx.send(m).await
}

#[tokio::main]
async fn main() {
    let (tx, mut rx) = mpsc::channel(100);
    let mut rng = rand::thread_rng();
    for i in 0..10 {
        let a = Message {
            id: i.to_owned(),
            sleep: rng.gen_range(100, 500).to_owned(),
            itr: 1,
        };
        tokio::spawn(sleeper(a, tx.clone()));
    }

    loop {
        if let Some(v) = rx.recv().await {
            println!("{:?}", v);
            let m = Message {
                id: v.id,
                sleep: rng.gen_range(100, 500).to_owned(),
                itr: v.itr + 1,
            };
            tokio::spawn(sleeper(m, tx.clone()));
        }
    }
}
